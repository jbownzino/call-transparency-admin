import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

//Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './app/header/header.component';
import { LoginComponent } from './app/login/login.component';
import { SetPasswordComponent } from './app/password/set-password.component';
import { ForgotPasswordComponent } from './app/password/forgot-password.component';
import { RegistrantsComponent } from './app/registrants/registrants.component';
import { SubmissionsComponent } from './app/submissions/submissions.component';
import { DetailComponent } from './app/detail/detail.component';
import { ListComponent } from './app/list/list.component';
import { VerifyComponent } from './app/verify/verify.component';

//Services
import { DataService} from './data.service';
import { UserLoginService} from './authentication.service';
import { CookieService } from 'ngx-cookie-service';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegistrantsComponent,
        SubmissionsComponent,
        HeaderComponent,
        DetailComponent,
        ListComponent,
        VerifyComponent,
        SetPasswordComponent,
        ForgotPasswordComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [DataService, UserLoginService, CookieService],
    bootstrap: [AppComponent]
})
export class AppModule { }
