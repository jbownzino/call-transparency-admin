import {Injectable} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from "@angular/router";
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, map, tap} from 'rxjs/operators';
import {Login, Registrants, Network, NewPasswordUser, ConfirmNewPassword} from './globalConstants';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class UserLoginService {
    private basePath: string = '';
    private idToken: string = '';
    private httpOptions: any;
    private subject = new Subject<any>();

    constructor(private http: HttpClient,
                private router: Router,
                private cookieService: CookieService) {
        this.basePath = Network.basePath;
    }

    /** POST: log the user in on Login screen -- we have to use AWS Cognito SDK for this: https://github.com/aws/amazon-cognito-identity-js */
    authenticate(login: Login) {
        return this.http.post(this.basePath + 'admin/authenticate', login).toPromise();
    }

    /** POST: Send a confirmation code because user wants to reset their password */
    forgotPassword(email: any) {
        return this.http.post(this.basePath + 'admin/forgotpassword', email).toPromise();
    }

    /** POST: Set new password on first login for new users */
    newPassword(data: NewPasswordUser) {
        return this.http.post(this.basePath + 'admin/challenge', data).toPromise();
    }

    /** POST: Confirm new password of user when entering new password (using "Forgot Password" flow) */
    confirmNewPassword(data: ConfirmNewPassword) {
        return this.http.post(this.basePath + 'admin/confirmforgot', data).toPromise();
    }

    /** Log the user out */
    logout() {
        this.cookieService.deleteAll();
        this.router.navigate(['/login']);
    }
}