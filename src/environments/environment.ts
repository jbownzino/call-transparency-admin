// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userPoolId: 'us-east-1_7O5znfIO0',
  identityPoolId: '785436337397',
  clientId: '57ug2nc9d24uh91teoo5cqm69t',
  region: 'us-east-1',
  cognito_idp_endpoint: null
};
