import {Component, OnInit, Input, Output} from '@angular/core';
import { Router, RouterLink } from "@angular/router";
import { NgModel } from '@angular/forms';
import { DataService } from '../../data.service';

@Component({
    selector: 'verify',
    templateUrl: './verify.component.html',
    styleUrls: [ './verify.component.css' ]
})

export class VerifyComponent implements OnInit {
    public choice: boolean = false;
    public verifyText: string = 'Verify';
    public comments: string = '';
    public radio: string = "2";
    public success: boolean = false;
    private submission: any = {};

    @Input() registrants: boolean = true;
    @Input() selected: any = {};

    constructor(private dataService: DataService,
                private router: Router) {}

    ngOnInit() {}

    public updateRadio = (choice:string):void => {
        this.choice = choice === "1" ? true : false;
    }

    public updateTextarea = (co:string):void => {
        this.comments = co;
    }

    public verify = ():void => {
        this.submission = {validated: this.choice, validatingUserEmail: this.selected.email, comment: this.comments};
        if (this.registrants) {
            this.verifyRegistrant();
        } else if (!this.registrants) {
            this.verifySubmission();
        }
    }

    private verifyRegistrant = ():void => {
        this.submission.registrantId = this.selected.registrantId;
        this.dataService.verifyRegistrant(this.submission)
            .then(response => {
                this.successAnimate();
                this.resetInputs();
                this.dataService.sendMessage('Refresh Registrants');
            }, response => {
                //Need to flesh out error codes.
                if (response.status === 0) {this.dataService.logout()}
            })
    }

    public verifySubmission = ():void => {
        this.submission.submissionId = this.selected.submissionId;
        this.dataService.verifySubmission(this.submission)
            .then(response => {
                this.successAnimate();
                this.resetInputs();
                this.dataService.sendMessage('Refresh Submissions');
            }, response => {
                if (response.status === 0) {this.dataService.logout()}
            })
    }

    private resetInputs = ():void => {
        this.comments = '';
        this.radio = "2";
    }

    private successAnimate = ():void => {
        this.success = true;
        this.verifyText = 'Submitted';
        setTimeout(() => {
            this.verifyText = 'Verify';
            this.success = false;
        }, 3000);
    }
}