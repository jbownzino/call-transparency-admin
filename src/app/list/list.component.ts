import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../data.service';
import {Router} from "@angular/router";

@Component({
    selector: 'list',
    templateUrl: './list.component.html',
    styleUrls: [ './list.component.css' ]
})

export class ListComponent implements OnInit {
    public registrants: boolean = false;
    public selected: number = 0;

    @Input() list: any = {}
    @Input() loading: boolean;
    @Output() populate: EventEmitter<Function> = new EventEmitter<Function>();

    constructor(private router: Router,
                private dataService: DataService) {}

    ngOnInit() {
        this.setRoute();
    }

    public setRoute = () => {
        this.registrants = this.router.url === '/registrants' ? true : false;
    }

    emitDetail(item:any, i:number) {
        this.populate.emit(item);
    }

    setActive(i:number) {
        this.selected = i;
    }
}