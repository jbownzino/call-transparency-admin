import { Component, OnInit, Input } from '@angular/core';
import {Router} from "@angular/router";
import * as moment from 'moment';

@Component({
    selector: 'detail',
    templateUrl: './detail.component.html',
    styleUrls: [ './detail.component.css' ]
})

export class DetailComponent implements OnInit {
    public screenType: string = 'registrant';
    public registrants: boolean = true;
    private now: any;

    @Input() detail: any = {}
    @Input() loading: boolean;

    constructor(private router: Router) {}

    ngOnInit() {
        this.setRoute();
        this.now = moment().format();
        console.log(this.now);
    }

    public formatDateTime = (date:string) => {
        return moment(date).format('LLL');
    }

    public getBusinessSize = (biz:any) => {
        return biz === 'bigBusiness' ? 'Service Provider' : 'Internal Employee';
    }

    public setRoute = () => {
        this.registrants = this.router.url === '/registrants' ? true : false;
    }

    public getLabel = () => {
        return this.router.url === '/submissions' ? 'submission' : 'registrant';
    }
}
