import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from "@angular/router";
import { NgModel } from '@angular/forms';
import { DataService } from '../../data.service';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: [ './header.component.css' ]
})

export class HeaderComponent implements OnInit {
    public registrants: boolean = true;
    public registrantCount: number = 0;
    public submissionCount: number = 0;
    public message: any = {};
    private subscription: Subscription;

    constructor(private dataService: DataService,
                private cookieService: CookieService,
                private router: Router) {
        this.subscription = this.dataService.getMessage()
            .subscribe(message => {this.chooseEventFunction(message.text)});
    }

    ngOnInit() {
        this.registrants = this.setRoute();
        this.getAllRegistrants();
        this.getAllSubmissions();
    }

    private chooseEventFunction = (message:string) => {
        switch (message) {
            case 'Refresh Registrants':
                this.getAllRegistrants();
                break;
            case 'Refresh Submissions':
                this.getAllSubmissions();
                break;
        }
    }

    private getAllRegistrants = () => {
        this.dataService.getRegistrants()
            .subscribe(r => {
                this.registrantCount = r['results'].length;
                console.log('New Registrant Count: ' + JSON.stringify(r['results']));
            });
    }

    private getAllSubmissions = () => {
        this.dataService.getSubmissions()
            .subscribe(r => {
                this.submissionCount = r['results'].length;
                console.log('New Submission Count: ' + JSON.stringify(r['results']));
            });
    }

    //Handle application of active CSS class in header
    private setRoute = () => {
        let registrants = true;
        if (this.router.url === '/submissions') {
            registrants = false;
        }
        return registrants;
    }

    public logout = () => {
        this.dataService.logout();
    }
}