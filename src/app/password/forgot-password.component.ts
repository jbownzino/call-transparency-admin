import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgModel } from '@angular/forms';
import { UserLoginService } from '../../authentication.service';
import { DataService } from '../../data.service';
import { ConfirmNewPassword } from '../../globalConstants';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'reset-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: [ './forgot-password.component.css' ]
})
export class ForgotPasswordComponent implements OnInit {
    private username: string = '';
    private newPassword: string = '';
    private code: string = '';
    public errorMessage: string = '';
    public userMessage: string = 'Please enter your registered e-mail below. We’ll email you instructions to reset your password.';
    public codeSent: boolean = false;
    public passwordReset: boolean = false;
    public passwordWeak: boolean = false;
    public requiredCode: boolean = false;
    public requiredUsername: boolean = false;
    public requiredNewPassword: boolean = false;

    constructor(private userService: UserLoginService,
                private dataService: DataService,
                private cookieService: CookieService,
                private router: Router) {}

    ngOnInit() {}

    private emptyUsername = () => {
        let empty = false;
        if (!this.username.length) {
            this.requiredUsername = true;
            empty = true;
        }
        return empty;
    }

    private emptyPasswordReset = () => {
        let empty = false;
        if (!this.newPassword.length) {
            this.requiredNewPassword = true;
            empty = true;
        }
        if (!this.code.length) {
            this.requiredCode = true;
            empty = true;
        }
        return empty;
    }

    private handleCodeSuccess = (response):void => {
        if (response.data) {
            this.codeSent = true;
            this.userMessage = 'A code has been sent to your e-mail. You must enter the code to reset your password';
        }
    }

    private handleCodeError = (response):void => {
        if (response) {
            this.errorMessage = 'An error occurred. Please try your request again.';
        }
    }


    private handlePasswordSuccess = (response):void => {
        if (response.data) {
            this.passwordReset = true;
            this.userMessage = 'Your password has been reset. Please wait while we redirect you to the login screen';
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 4000);
        }
    }

    private handlePasswordError = (response):void => {
        if (response) {
            this.errorMessage = 'An error occurred. Please try your request again.';
        }
    }

    public checkPasswordStrength = (pw) => {
        if (pw.length) {
            this.requiredNewPassword = false;
        }
        this.passwordWeak = !pw.match("^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*\\d)(?=\\S*[^\\w\\s])\\S{8,}$") ? true : false;
    };

    public forgotPassword = ():void => {
        if (this.emptyUsername()) {
            console.log('Input values are required');
        } else {
            this.userService.forgotPassword({email: this.username})
                .then(response => {
                    this.cookieService.set('username', this.username);
                    this.errorMessage = '';
                    this.handleCodeSuccess(response);
                }, response => {
                    this.handleCodeError(response)
                })
        }
    }

    public confirmNewPassword = ():void => {
        const username = this.cookieService.get('username');
        if (this.emptyPasswordReset()) {
            console.log('Input values are required');
        } else {
            this.userService.confirmNewPassword(new ConfirmNewPassword(this.username, this.newPassword, this.code))
                .then(response => {
                    this.handlePasswordSuccess(response);
                }, response => {
                    this.handlePasswordError(response)
                })
        }
    }
}
