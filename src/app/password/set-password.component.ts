import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgModel } from '@angular/forms';
import { UserLoginService } from '../../authentication.service';
import { Login, NewPasswordUser } from '../../globalConstants';
import { LoginResponse } from '../../globalConstants';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'password',
    templateUrl: './set-password.component.html',
    styleUrls: [ './set-password.component.css' ]
})
export class SetPasswordComponent implements OnInit {
    public username: string = '';
    public password: string = '';
    public confirmPassword: string = '';
    public existingPassword: string = '';
    public session: string = '';
    public errorMessage: string = '';
    public userMessage: string = '';
    public requiredPassword: boolean = false;
    public requiredNewPassword: boolean = false;
    public requiredConfirmPassword: boolean = false;
    public passwordMismatch: boolean = false;
    public passwordWeak: boolean = false;
    public noPasswordMatch: boolean = false;
    public requiredError: boolean = false;

    constructor(private userService: UserLoginService,
                private cookieService: CookieService,
                private router: Router) {
        this.username = this.cookieService.get('username');
        this.session = this.cookieService.get('session');
    }

    ngOnInit() {}

    public checkEmpty = (pw) => {
        if (pw.length) {
            this.requiredPassword = false;
        }
    };

    public checkPasswordStrength = (pw) => {
        if (pw.length) {
            this.requiredNewPassword = false;
        }
        this.checkPasswordMatch(pw);
        this.passwordWeak = !pw.match("^(?=\\S*[a-z])(?=\\S*[A-Z])(?=\\S*\\d)(?=\\S*[^\\w\\s])\\S{8,}$") ? true : false;
    };

    public checkPasswordMatch = (pw) => {
        if (pw.length) {
            this.requiredConfirmPassword = false;
        }
        this.passwordMismatch = this.password !== this.confirmPassword ? true : false;

    };

    public isInvalid = () => {
        let ans = false;
        if (this.passwordMismatch && this.passwordWeak && this.requiredPassword && this.requiredNewPassword) {
            ans = true;
        }
        return ans;
    }

    private emptyInputs = () => {
        let empty = false;
        if (!this.password.length) {
            this.requiredNewPassword = true;
            empty = true;
        }
        if (!this.existingPassword.length) {
            this.requiredPassword = true;
            empty = true;
        }
        if (!this.confirmPassword.length) {
            this.requiredConfirmPassword = true;
            empty = true;
        }
        return empty;
    };

    public setPassword = ():void => {
        if (this.emptyInputs()) {
            console.log('Invalid inputs detected');
        } else {
            this.userService.newPassword(new NewPasswordUser(this.username, this.existingPassword, this.password, this.session))
                .then(response => {
                    this.handleSuccess(response);
                }, response => {
                    this.handleError(response);
                })
        }
    };

    private handleError = (response:any) => {
        this.errorMessage = 'An error occurred. Please try your request again.';
    }

    private handleSuccess = (response:any) => {
        if (response.token) {
            this.userMessage = 'Password Successfully Reset! You will now be redirected to the login screen.';
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 4000);
        }
    }
}
