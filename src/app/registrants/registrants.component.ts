import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Router } from "@angular/router";
import { NgModel } from '@angular/forms';
import { DataService } from '../../data.service';
import { HeaderComponent } from '../header/header.component';
import { ListComponent } from '../list/list.component';
import { DetailComponent } from '../detail/detail.component';

@Component({
    selector: 'registrants',
    templateUrl: './registrants.component.html',
    styleUrls: [ './registrants.component.css' ]
})

export class RegistrantsComponent implements OnInit {
    public details: any;
    public detail: any;
    public selected: number = null;
    public title: string = '';
    public loading: boolean = true;
    public message: any = {};
    private subscription: Subscription;

    constructor(private dataService: DataService,
                private router: Router) {
        //Subscription to messaging service which looks at verify call in VerifyComponent and tells
        //Whether to call for getRegistrants and refresh the list
        this.subscription = this.dataService.getMessage().subscribe(message => { if (message.text === 'Refresh Registrants') {this.getAllRegistrants()}});
    };

    ngOnInit() {
        this.getAllRegistrants();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public onPopulate = (r: any): void => {
        this.detail = r;
    }

    private getAllRegistrants = () => {
        this.dataService.getRegistrants()
            .subscribe(r => {
                    this.details = r['results'].sort((d1, d2) => new Date(d1.createdDateTime).getTime() - new Date(d2.createdDateTime).getTime());
                    this.detail = r['results'][0];
                    this.loading = false;
                },
                    err => {
                if (err.status === 0) {
                    this.dataService.logout()
                }});
    }
}
