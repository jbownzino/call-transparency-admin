import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgModel } from '@angular/forms';
import { UserLoginService } from '../../authentication.service';
import { DataService } from '../../data.service';
import { Login } from '../../globalConstants';
import { LoginResponse } from '../../globalConstants';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
    public username: string = '';
    public password: string = '';
    public errorMessage: string = '';
    public userMessage: string = '';
    public requiredUsername: boolean = false;
    public requiredPassword: boolean = false;

    constructor(private userService: UserLoginService,
                private dataService: DataService,
                private cookieService: CookieService,
                private router: Router) {}

    ngOnInit() {}

    public login = ():void => {
        if (this.emptyInputs()) {
            console.log('Input values are required');
        } else {
            this.userService.authenticate(new Login(this.username, this.password))
                .then(response => {
                    this.handleSuccess(response);
                }, response => {
                    this.handleError(response)
                })
        }
    }

    private emptyInputs = () => {
        let empty = false;
        if (!this.username.length) {
            this.requiredUsername = true;
            empty = true;
        }
        if (!this.password.length) {
            this.requiredPassword = true;
            empty = true;
        }
        return empty;
    }

    private handleSuccess = (response):void => {
        if (response.token) {
            this.cookieService.set('idToken', response.token.idToken);
            this.router.navigate(['/registrants']);
        }
        if (response.challenge) {
            this.cookieService.set('session', response.session);
            this.cookieService.set('username', this.username);
            this.errorMessage = 'A new password must be set. You will now be redirected to the set password screen.'
            setTimeout(() => {
                this.router.navigate(['/set-password']);
            }, 4000);
        }
    }

    private handleError = (response):void => {
        if (response.status === 410) {
            this.errorMessage = 'A new password must be set. You will now be redirected to the set password screen.'
            this.cookieService.set('username', this.username);
            setTimeout(() => {
                this.router.navigate(['/set-password']);
            }, 4000);
        }
        if (response.status === 409) {
            this.errorMessage = 'Invalid Username or Password'
        }
    }
}
