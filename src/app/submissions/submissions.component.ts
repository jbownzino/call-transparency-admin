import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { NgModel } from '@angular/forms';
import { DataService } from '../../data.service';
import { CookieService } from 'ngx-cookie-service';
import { HeaderComponent } from '../header/header.component';
import { ListComponent } from '../list/list.component';
import { DetailComponent } from '../detail/detail.component';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'submissions',
    templateUrl: './submissions.component.html',
    styleUrls: [ './submissions.component.css' ]
})

export class SubmissionsComponent implements OnInit {
    public details: any;
    public detail: any;
    public selected: number = null;
    public loading: boolean = true;
    public message: any = {};
    private subscription: Subscription;

    constructor(private dataService: DataService,
                private cookieService: CookieService,
                private router: Router) {
        //Subscription to messaging service which looks at verify call in VerifyComponent and tells
        //Whether to call for getSubmissions and refresh the list
        this.subscription = this.dataService.getMessage().subscribe(message => { if (message.text === 'Refresh Submissions') {this.getAllSubmissions()}});
    }

    ngOnInit() {
        this.getAllSubmissions();
        console.log(this.loading);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public onPopulate = (r:any):void => {
        this.detail = r;
    }

    private getAllSubmissions = () => {
        this.dataService.getSubmissions()
            .subscribe(r => {
                this.details = r['results'].sort((d1, d2) => new Date(d1.createdDateTime).getTime() - new Date(d2.createdDateTime).getTime());
                this.detail = r['results'][0];
                    this.loading = false;
                }, err => {
                if (err.status === 0) {
                    this.dataService.logout()
                }
            });
    }

}
