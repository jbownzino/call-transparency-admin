import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './app/login/login.component';
import { SetPasswordComponent } from './app/password/set-password.component';
import { ForgotPasswordComponent } from './app/password/forgot-password.component';
import { RegistrantsComponent } from './app/registrants/registrants.component';
import { SubmissionsComponent } from './app/submissions/submissions.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'registrants', component: RegistrantsComponent },
    { path: 'submissions', component: SubmissionsComponent },
    { path: 'set-password', component: SetPasswordComponent },
    { path: 'reset-password', component: ForgotPasswordComponent }
];

export const AppRoutingModule = RouterModule.forRoot(routes);