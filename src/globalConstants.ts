/** All of our classes/interfaces for the app will reside here */
export class Network {
    public static basePath = 'https://hlt2ufz4n4.execute-api.us-east-1.amazonaws.com/rapid/';
}

export class Login {
    email: string;
    password: string;

    constructor(username: string, password: string) {
        const login = {
            email: username,
            password: password
        }
        return login;
    }
}

export interface Registrants {
    contactName: string;
    contactWebsite: string;
    contactPhoneNumber: string;
    contactBusinessSize: string;
    contactIndustry: string;
    firstName: string;
    lastName: string;
    email: string;
    registrantId: string;
}

export class NewPasswordUser {
    email: string;
    oldPassword: string;
    newPassword: string;
    session: string;

    constructor(username: string, oldPw: string, newPw: string, sessionId: string) {
        const newPassword = {
            email: username,
            oldPassword: oldPw,
            newPassword: newPw,
            session: sessionId
        }
        return newPassword;
    }
}

export class ConfirmNewPassword {
    email: string;
    password: string;
    confirmationCode: string;

    constructor(username: string, newPw: string, code: string) {
        const newPassword = {
            email: username,
            password: newPw,
            confirmationCode: code
        }
        return newPassword;
    }
}

export class LoginResponse {
    data: {
       companyInfo: {
            name: "Jared",
            website: "www.google.com",
            phoneNumber: "8606343235",
            businessSize: "smallBusiness"
        },
        firstName: "Jared",
        lastName: "Bowns",
        email: "jared2@elyxor.com",
        userId: "6239821e-b005-44d8-a1a0-7cadac9845a1",
        registrantId: "96878d89-6903-4eec-8f86-1f83fb0cea7e",
        registrant: true,
        verfified: false
    }
}