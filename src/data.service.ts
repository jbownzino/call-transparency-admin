import {Injectable} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from "@angular/router";
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, map, tap} from 'rxjs/operators';
import {Login, Registrants, Network} from './globalConstants';
import { CookieService } from 'ngx-cookie-service';
import { AuthenticationDetails, CognitoUser, CognitoUserSession } from "amazon-cognito-identity-js";

@Injectable()
export class DataService {
    private basePath: string = '';
    private idToken: string = '';
    private httpOptions: any;
    private subject = new Subject<any>();

    constructor(private http: HttpClient,
                private router: Router,
                private cookieService: CookieService) {
        this.basePath = Network.basePath;
    }

    createHttpOptions():void {
        this.idToken = this.cookieService.get('idToken');
        this.httpOptions = {
            headers: new HttpHeaders({'X-Auth-Token': this.idToken})
        };
    }

    /** GET Messaging service method to ship messages around between components */
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    sendMessage(message: string) {
        this.subject.next({ text: message });
    }

    /** GET All Unverified Registrants */
    getRegistrants() {
        this.createHttpOptions();
        return this.http.get(this.basePath + 'registrant?validated=null', this.httpOptions);
    }

    /** GET All Unverified Submissions */
    getSubmissions() {
        this.createHttpOptions();
        return this.http.get(this.basePath + 'submission?validated=null', this.httpOptions);
    }

    /** PUT Verify Registrant */
    verifyRegistrant<Data>(reg:any) {
        this.createHttpOptions();
        return this.http.put(this.basePath + 'registrant', reg, this.httpOptions).toPromise()
    }

    /** PUT Verify Submission */
    verifySubmission<Data>(sub:any) {
        this.createHttpOptions();
        return this.http.put(this.basePath + 'submission', sub, this.httpOptions).toPromise()
    }

    /** Log the user out */
    logout() {
        this.cookieService.deleteAll();
        this.router.navigate(['/login']);
    }
}